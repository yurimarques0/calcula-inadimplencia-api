package com.example.calculainadimplenciaapi.enums;

public enum TipoPlanoPagamento {
    NO("NO"), FS("FS"), FQ("FQ"), FM("FM");

    private final String name;

    private TipoPlanoPagamento(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
