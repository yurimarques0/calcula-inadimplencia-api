package com.example.calculainadimplenciaapi.enums;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum UF {
	@JsonProperty("AC")
	AC("AC"),
	@JsonProperty("AL")
	AL("AL"),
	@JsonProperty("AP")
	AP("AP"),
	@JsonProperty("AM")
	AM("AM"),
	@JsonProperty("BA")
	BA("BA"),
	@JsonProperty("CE")
	CE("CE"),
	@JsonProperty("DF")
	DF("DF"),
	@JsonProperty("ES")
	ES("ES"),
	@JsonProperty("GO")
	GO("GO"),
	@JsonProperty("MA")
	MA("MA"),
	@JsonProperty("MT")
	MT("MT"),
	@JsonProperty("MS")
	MS("MS"),
	@JsonProperty("MG")
	MG("MG"),
	@JsonProperty("PA")
	PA("PA"),
	@JsonProperty("PB")
	PB("PB"),
	@JsonProperty("PR")
	PR("PR"),
	@JsonProperty("PE")
	PE("PE"),
	@JsonProperty("PI")
	PI("PI"),
	@JsonProperty("RJ")
	RJ("RJ"),
	@JsonProperty("RN")
	RN("RN"),
	@JsonProperty("RS")
	RS("RS"),
	@JsonProperty("RO")
	RO("RO"),
	@JsonProperty("RR")
	RR("RR"),
	@JsonProperty("SC")
	SC("SC"),
	@JsonProperty("SP")
	SP("SP"),
	@JsonProperty("SE")
	SE("SE"),
	@JsonProperty("TO")
	TO("TO");

	private final String name;

	private UF(String uf) {
		this.name = uf;
	}

	@Override
	public String toString() {
		return this.name;
	}
}
