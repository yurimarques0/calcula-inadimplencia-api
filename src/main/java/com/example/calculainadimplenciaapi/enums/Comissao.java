package com.example.calculainadimplenciaapi.enums;

public enum Comissao {
    V("V"), N("N");

    private final String name;

    private Comissao(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
