package com.example.calculainadimplenciaapi.enums;

public enum SimNao {
	N("N"), S("S");

	private final String name;

	private SimNao(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
}
