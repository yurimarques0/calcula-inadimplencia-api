package com.example.calculainadimplenciaapi.enums;

public enum TipoInadimplencia {
    GERAL("GERAL"), MENSAL("MENSAL"), ANUAL("ANUAL");

    private final String name;

    private TipoInadimplencia(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
