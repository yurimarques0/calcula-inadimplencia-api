package com.example.calculainadimplenciaapi.enums;

public enum FisicaJuridica {
	F("F"), J("J");

	private final String name;

	private FisicaJuridica(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
}
