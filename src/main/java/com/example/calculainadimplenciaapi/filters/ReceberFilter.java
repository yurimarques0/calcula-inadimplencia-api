package com.example.calculainadimplenciaapi.filters;

import com.example.calculainadimplenciaapi.enums.TipoInadimplencia;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ReceberFilter {
    private TipoInadimplencia tipoInadimplencia;
}
