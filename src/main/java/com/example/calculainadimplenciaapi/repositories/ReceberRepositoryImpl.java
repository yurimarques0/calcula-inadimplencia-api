package com.example.calculainadimplenciaapi.repositories;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;

import com.example.calculainadimplenciaapi.dto.out.ReceberDTOout;
import com.example.calculainadimplenciaapi.enums.TipoInadimplencia;
import com.example.calculainadimplenciaapi.filters.ReceberFilter;
import com.example.calculainadimplenciaapi.models.Receber;
import com.example.calculainadimplenciaapi.models.Receber_;

public class ReceberRepositoryImpl implements ReceberRepositoryQuery {
    @Autowired
    private EntityManager manager;

    @Override
    public List<ReceberDTOout> titulosEmAberto(ReceberFilter filter) {
        CriteriaBuilder cb = manager.getCriteriaBuilder();

        CriteriaQuery<ReceberDTOout> criteriaQuery = cb.createQuery(ReceberDTOout.class);
        Root<Receber> rootFrom = criteriaQuery.from(Receber.class);

        addFields(cb, criteriaQuery, rootFrom);

        Predicate[] predicates = addRestrictionsTitulosEmAberto(filter, cb, rootFrom);
        criteriaQuery.where(predicates);
        TypedQuery<ReceberDTOout> query = manager.createQuery(criteriaQuery);
        return query.getResultList();
    }

    private Predicate[] addRestrictionsTitulosEmAberto(ReceberFilter filter, CriteriaBuilder cb,
            Root<Receber> rootFrom) {
        List<Predicate> predicates = new ArrayList<>();

        Predicate condition = null;
        Predicate condition2 = null;

        if (filter.getTipoInadimplencia() != null && filter.getTipoInadimplencia() == TipoInadimplencia.MENSAL) {
            condition = cb.between(rootFrom.get(Receber_.DT_VENCTO), LocalDate.now().withDayOfMonth(1),
                    LocalDate.now());
            predicates.add(condition);
        }
        if (filter.getTipoInadimplencia() != null && filter.getTipoInadimplencia() == TipoInadimplencia.ANUAL) {
            condition = cb.between(rootFrom.get(Receber_.DT_VENCTO), LocalDate.now().withDayOfYear(1),
                    LocalDate.now());
            predicates.add(condition);
        }

        condition2 = cb.equal(rootFrom.get(Receber_.VALOR_PAGO), 0);
        predicates.add(condition2);

        return predicates.toArray(new Predicate[predicates.size()]);
    }

    @Override
    public List<ReceberDTOout> totalReceber(ReceberFilter filter) {
        CriteriaBuilder cb = manager.getCriteriaBuilder();

        CriteriaQuery<ReceberDTOout> criteriaQuery = cb.createQuery(ReceberDTOout.class);
        Root<Receber> rootFrom = criteriaQuery.from(Receber.class);

        addFields(cb, criteriaQuery, rootFrom);

        Predicate[] predicates = addRestrictionsTotalReceber(filter, cb, rootFrom);
        criteriaQuery.where(predicates);
        TypedQuery<ReceberDTOout> query = manager.createQuery(criteriaQuery);
        return query.getResultList();
    }

    private Predicate[] addRestrictionsTotalReceber(ReceberFilter filter, CriteriaBuilder cb, Root<Receber> rootFrom) {
        List<Predicate> predicates = new ArrayList<>();

        Predicate condition = null;

        if (filter.getTipoInadimplencia() != null && filter.getTipoInadimplencia() == TipoInadimplencia.MENSAL) {
            condition = cb.between(rootFrom.get(Receber_.DT_VENCTO), LocalDate.now().withDayOfMonth(1),
                    LocalDate.now());

            predicates.add(condition);
        }
        if (filter.getTipoInadimplencia() != null && filter.getTipoInadimplencia() == TipoInadimplencia.ANUAL) {
            condition = cb.between(rootFrom.get(Receber_.DT_VENCTO), LocalDate.now().withDayOfYear(1),
                    LocalDate.now());
            predicates.add(condition);
        }

        return predicates.toArray(new Predicate[predicates.size()]);
    }

    private void addFields(CriteriaBuilder cb, CriteriaQuery<ReceberDTOout> criteriaQuery, Root<Receber> rootFrom) {
        criteriaQuery.select(
                cb.construct(ReceberDTOout.class,
                        rootFrom.get(Receber_.VALOR)//
                ));
    }
}
