package com.example.calculainadimplenciaapi.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.calculainadimplenciaapi.models.Receber;

@Repository
public interface ReceberRepository extends JpaRepository<Receber, Long>, ReceberRepositoryQuery {
}
