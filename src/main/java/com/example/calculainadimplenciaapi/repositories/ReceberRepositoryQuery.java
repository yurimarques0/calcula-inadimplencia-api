package com.example.calculainadimplenciaapi.repositories;

import java.util.List;

import com.example.calculainadimplenciaapi.dto.out.ReceberDTOout;
import com.example.calculainadimplenciaapi.filters.ReceberFilter;

public interface ReceberRepositoryQuery {
    public List<ReceberDTOout> titulosEmAberto(ReceberFilter filter);

    public List<ReceberDTOout> totalReceber(ReceberFilter filter);
}