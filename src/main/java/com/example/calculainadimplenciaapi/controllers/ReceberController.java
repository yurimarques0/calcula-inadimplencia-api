package com.example.calculainadimplenciaapi.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.calculainadimplenciaapi.filters.ReceberFilter;
import com.example.calculainadimplenciaapi.services.ReceberService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import com.example.calculainadimplenciaapi.config.RestControllerPath;

@RestController
@RequestMapping(RestControllerPath.RECEBER_PATH)
@Api(tags = "Receber", description = "Receber")
public class ReceberController {

    @Autowired
    private ReceberService service;

    @ApiOperation(value = "Calcular inadimplência")
    @GetMapping
    public Double calcularInadimplencia(ReceberFilter filter) {
        return service.calcularInadimplencia(filter);
    }
}
