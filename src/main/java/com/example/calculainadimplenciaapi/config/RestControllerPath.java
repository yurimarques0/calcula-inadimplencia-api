package com.example.calculainadimplenciaapi.config;

public final class RestControllerPath {

    ///////////////////////////////////////////////////////////////
    // ROOT PATH
    ///////////////////////////////////////////////////////////////
    public static final String ALL = "/**";
    public static final String ROOT_PATH = "/api";
    public static final String PUBLIC_ROOT_PATH = ROOT_PATH + "/public";
    public static final String PRIVATE_ROOT_PATH = ROOT_PATH + "/private";

    ///////////////////////////////////////////////////////////////
    // PRIVATE PATHS
    ///////////////////////////////////////////////////////////////
    public static final String MARCA_PATH = PRIVATE_ROOT_PATH + "/marcas";
    public static final String COLECAO_PATH = PRIVATE_ROOT_PATH + "/colecoes";
    public static final String COR_PATH = PRIVATE_ROOT_PATH + "/cores";
    public static final String EMPRESA_PATH = PRIVATE_ROOT_PATH + "/empresas";
    public static final String PRODUTO_PATH = PRIVATE_ROOT_PATH + "/produtos";
    public static final String SECAO_PATH = PRIVATE_ROOT_PATH + "/secoes";
    public static final String TAMANHO_PATH = PRIVATE_ROOT_PATH + "/tamanhos";
    public static final String LOCAL_ESTOQUE_PATH = PRIVATE_ROOT_PATH + "/locais-de-estoque";
    public static final String UNIDADE_PATH = PRIVATE_ROOT_PATH + "/unidades";
    public static final String CLIENTE_PATH = PRIVATE_ROOT_PATH + "/clientes";
    public static final String VENDEDOR_PATH = PRIVATE_ROOT_PATH + "/vendedores";
    public static final String SUPERVISOR_PATH = PRIVATE_ROOT_PATH + "/supervisores";
    public static final String PLANO_PAGAMENTO_PATH = PRIVATE_ROOT_PATH + "/planos-de-pagamento";
    public static final String CIDADE_PATH = PRIVATE_ROOT_PATH + "/cidades";
    public static final String CATEGORIA_PATH = PRIVATE_ROOT_PATH + "/categorias";
    public static final String PARAMETER_PATH = PRIVATE_ROOT_PATH + "/parametros";
    public static final String PARAMETER_APPLICATION_PATH = PRIVATE_ROOT_PATH + "/parametros-aplicacao";
    public static final String USER_PATH = PRIVATE_ROOT_PATH + "/usuarios";
    public static final String TABELA_PRECO_PATH = PRIVATE_ROOT_PATH + "/tabela-preco";
    public static final String VARIEDADE_PATH = PRIVATE_ROOT_PATH + "/variedades";
    public static final String GRADE_PATH = PRIVATE_ROOT_PATH + "/grades";
    public static final String APLICACAO_TABELA_PRECO_PATH = PRIVATE_ROOT_PATH + "/aplicacao-tabela-preco";
    public static final String PRE_PEDIDO_PATH = PRIVATE_ROOT_PATH + "/pre-pedido";
    public static final String PEDIDO_PATH = PRIVATE_ROOT_PATH + "/pedido";
    public static final String IMAGEM_PRODUTO_PATH = PRIVATE_ROOT_PATH + "/imagem-produto";
    public static final String PRODUTO_VARIACAO_PATH = PRIVATE_ROOT_PATH + "/produto-variacao";
    public static final String RECEBER_PATH = PRIVATE_ROOT_PATH + "/receber";

    public static final String BACKUP_PATH = PRIVATE_ROOT_PATH + "/backup";
    public static final String RESTORE_PATH = PRIVATE_ROOT_PATH + "/restore";

    ///////////////////////////////////////////////////////////////
    // PUBLIC PATHS
    ///////////////////////////////////////////////////////////////
    public static final String LOGIN_PATH = PUBLIC_ROOT_PATH + "/oauth/token";
    public static final String LOGOUT_PATH = PUBLIC_ROOT_PATH + "/logout";
}
