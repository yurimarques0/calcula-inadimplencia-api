package com.example.calculainadimplenciaapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CalculaInadimplenciaApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CalculaInadimplenciaApiApplication.class, args);
	}

}
