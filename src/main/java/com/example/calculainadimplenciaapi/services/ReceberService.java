package com.example.calculainadimplenciaapi.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.calculainadimplenciaapi.filters.ReceberFilter;

@Service
public class ReceberService {

    @Autowired
    private Inadimplencia inadimplencia;

    public Double calcularInadimplencia(ReceberFilter filter) {
        return inadimplencia.executar(filter);
    }

}
