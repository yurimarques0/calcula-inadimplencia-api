package com.example.calculainadimplenciaapi.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.calculainadimplenciaapi.dto.out.ReceberDTOout;
import com.example.calculainadimplenciaapi.filters.ReceberFilter;
import com.example.calculainadimplenciaapi.repositories.ReceberRepository;

@Component
public class Inadimplencia {
    @Autowired
    private ReceberRepository repository;

    public Double executar(ReceberFilter filter) {
        return calcular(somar(repository.titulosEmAberto(filter)),
                somar(repository.totalReceber(filter)));
    }

    Double somar(List<ReceberDTOout> titulos) {
        Double total = 0.0;
        for (int i = 0; i < titulos.size(); i++) {
            total += titulos.get(i).getValor();
        }
        return total;
    }

    Double calcular(Double titulosEmAberto, Double totalReceber) {
        return totalReceber > 0 ? (titulosEmAberto / totalReceber) * 100 : 0D;
    }

}
