package com.example.calculainadimplenciaapi.models;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.hibernate.annotations.UpdateTimestamp;

import com.example.calculainadimplenciaapi.enums.TipoPlanoPagamento;

import lombok.Data;

@Entity(name = "Planpgto_tb")
@Data
public class PlanoPagamento {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "codigo_plp")
    private Long id;

    @NotBlank(message = "Obrigatório inserir a descrição do plano de pagamento")
    @Column(name = "descricao_plp", length = 50)
    private String descricao;

    @Size(min = 1, message = "Obrigatório ter ao menos um dia")
    @Column(name = "Dias_plp", length = 121)
    private String dias;

    @Column(name = "TipoPlanPgto_plp")
    @Enumerated(EnumType.STRING)
    private TipoPlanoPagamento tipo;

    @Column(name = "Acrescimo_plp")
    private Double acrescimo;

    @Column(name = "Desconto_plp")
    private Double desconto;

    @Column(name = "ValorMinimo_plp")
    private Double valorMinimmo;

    @UpdateTimestamp
    @Column(name = "UltimaGravacao_plp")
    private LocalDateTime lastChange;
}
