package com.example.calculainadimplenciaapi.models;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;

import org.hibernate.annotations.UpdateTimestamp;

import lombok.Data;

@Entity(name = "supervisores_tb")
@Data
public class Supervisor {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "codigo_sup")
    private Long id;

    @NotBlank(message = "Obrigatório informar o nome do supervisor")
    @Column(name = "nome_sup", length = 40)
    private String nome;

    @UpdateTimestamp
    @Column(name = "UltimaGravacao_sup")
    private LocalDateTime lastChange;
}
