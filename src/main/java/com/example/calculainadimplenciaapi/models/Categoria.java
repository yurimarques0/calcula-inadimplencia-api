package com.example.calculainadimplenciaapi.models;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;

import org.hibernate.annotations.UpdateTimestamp;

import lombok.Data;

@Data
@Entity(name = "CategoriasDeCliente_tb")
public class Categoria {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Codigo_CatCli")
    private Long id;

    @NotBlank(message = "Obrigatório informar a descrição da categoria")
    @Column(name = "Descricao_CatCli")
    private String descricao;

    @UpdateTimestamp
    @Column(name = "UltimaGravacao_CatCli")
    private LocalDateTime lastChange;

}
