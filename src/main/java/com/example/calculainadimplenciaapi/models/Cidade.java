package com.example.calculainadimplenciaapi.models;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.UpdateTimestamp;

import com.example.calculainadimplenciaapi.enums.UF;

import lombok.Data;

@Entity(name = "cidades_tb")
@Data
public class Cidade {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "codigo_cid")
    private Long id;

    @Column(name = "nome_cid")
    private String nome;

    @Column(name = "CodIbge_cid")
    private String codIBGE;

    @Column(name = "Latitude_cid")
    private Double latitude;

    @Column(name = "Longitude_cid")
    private Double longitude;

    @Column(name = "Cep_cid")
    @NotBlank(message = "Obrigatório informar o cep")
    private String cepDefault;

    @Enumerated(EnumType.STRING)
    @Column(name = "UF_cid")
    private UF uf;

    @ManyToOne
    @NotNull(message = "Obrigatório informar o pais")
    @JoinColumn(name = "codigo_pai", nullable = false)
    private Pais pais;

    @UpdateTimestamp
    @Column(name = "UltimaGravacao_cid")
    private LocalDateTime lastChange;
}
