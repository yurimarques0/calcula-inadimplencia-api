package com.example.calculainadimplenciaapi.models;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.example.calculainadimplenciaapi.enums.FisicaJuridica;
import com.example.calculainadimplenciaapi.enums.SimNao;

import lombok.Data;

@Entity(name = "clientes_tb")
@Data
public class Cliente {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "codigo_cli")
    private Long id;

    @Column(name = "Nome_cli")
    private String nome;

    @Column(name = "nomeFantasia_cli")
    private String nomeFantasia;

    @ManyToOne(optional = true)
    @JoinColumn(name = "Codigo_emp")
    private Empresa empresa;

    @Column(name = "Pessoa_cli")
    @Enumerated(EnumType.STRING)
    private FisicaJuridica tipo;

    @Column(name = "CpfCgc_cli")
    private String cpfCNPJ;

    @Column(name = "RgInsc_cli")
    private String rgIE;

    @Column(name = "Endereco_cli")
    private String endereco;

    @Column(name = "Bairro_cli")
    private String bairro;

    @ManyToOne(optional = true)
    @JoinColumn(name = "Codigo_cid")
    private Cidade cidade;

    @ManyToOne(optional = true)
    @JoinColumn(name = "Codigo_CatCli", nullable = true)
    private Categoria categoria;

    @ManyToOne(optional = true)
    @JoinColumn(name = "Codigo_plp", nullable = true)
    private PlanoPagamento planoPagamento;

    @Column(name = "Cep_cli")
    private String cep;

    @Column(name = "Fone1_cli")
    private String fone1;

    @Column(name = "Celular_cli")
    private String celular;

    @ManyToOne(optional = true)
    @JoinColumn(name = "Codigo_ven")
    private Vendedor vendedor;

    @Column(name = "PessoaContato_cli")
    private String pessoaContato;

    @Column(name = "Latitude_cli")
    private Double latitude;

    @Column(name = "Longitude_cli")
    private Double longitude;

    @Column(name = "ClienteInativo_cli")
    @Enumerated(EnumType.STRING)
    private SimNao inativo;

    @Column(name = "Email_cli")
    private String email;

    @Column(name = "Bloqueado_cli")
    @Enumerated(EnumType.STRING)
    private SimNao bloqueado;

    @Column(name = "DescricaoBlq_cli")
    private String descricaoDoBloqueio;

    @Column(name = "Transmitido_cli")
    @Enumerated(EnumType.STRING)
    private SimNao transmitido;

    @Column(name = "AtualizadoNoServidor_cli")
    @Enumerated(EnumType.STRING)
    private SimNao atualizado;

    @CreationTimestamp
    @Column(name = "DtCadastro_cli")
    private LocalDateTime registrationDate;

    @UpdateTimestamp
    @Column(name = "DtUltAlteracao_cli")
    private LocalDateTime lastChange;

    public void activate() {
        this.inativo = SimNao.N;
    }

    public void deactivate() {
        this.inativo = SimNao.S;
    }

}
