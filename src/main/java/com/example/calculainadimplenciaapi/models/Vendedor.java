package com.example.calculainadimplenciaapi.models;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotBlank;

import com.example.calculainadimplenciaapi.enums.Comissao;
import com.example.calculainadimplenciaapi.enums.SimNao;

import lombok.Data;

@Data
@Entity(name = "vendedores_tb")
public class Vendedor {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "codigo_ven")
    private Long id;

    @NotBlank(message = "Obrigatório informar o nome do vendedor")
    @Column(name = "nome_ven", length = 35)
    private String nome;

    @ManyToOne(optional = true)
    @JoinColumn(name = "codigo_sup")
    private Supervisor supervisor;

    @Enumerated(EnumType.STRING)
    @Column(name = "RecebeComissao_ven")
    private Comissao temComissao;

    @Enumerated(EnumType.STRING)
    @Column(name = "VendedorAtivo_ven")
    private SimNao active;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "CidadesVendedor_tb", joinColumns = @JoinColumn(name = "codigo_ven"), inverseJoinColumns = @JoinColumn(name = "codigo_cid"))
    private List<Cidade> cidades = new ArrayList<>();

    @Column(name = "UltimaGravacao_ven")
    private LocalDateTime lastChange;

}
