package com.example.calculainadimplenciaapi.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Entity(name = "paises_tb")
@Data
public class Pais {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "codigo_pai")
    private Long id;

    @Column(name = "Nome_pai")
    private String nome;

    @Column(name = "CodigoBACEN_pai")
    private Long codigoBACEN;

}
