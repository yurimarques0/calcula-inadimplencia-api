package com.example.calculainadimplenciaapi.models;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;

import org.hibernate.annotations.UpdateTimestamp;

import com.example.calculainadimplenciaapi.enums.SimNao;

import lombok.Data;

@Data
@Entity(name = "empresas_tb")
public class Empresa {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "codigo_emp")
    private Long id;

    @Column(name = "Razao_emp")
    @NotBlank(message = "Insira a razao social da empresa")
    private String razaoSocial;

    @Enumerated(EnumType.STRING)
    @Column(name = "Inativo_emp")
    private SimNao inactive;

    @UpdateTimestamp
    @Column(name = "UltimaGravacao_emp")
    private LocalDateTime lastChange;

    public void activate() {
        this.inactive = SimNao.N;
    }

    public void deactivate() {
        this.inactive = SimNao.S;
    }

}
