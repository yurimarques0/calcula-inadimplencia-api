package com.example.calculainadimplenciaapi.models;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "Receber_tb")
public class Receber {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Codigo_rec")
    private Long id;

    @ManyToOne(optional = true)
    @JoinColumn(name = "Codigo_emp")
    private Empresa empresa;

    @ManyToOne
    @JoinColumn(name = "codigo_cli", nullable = false)
    private Cliente cliente;

    @Column(name = "DataMov_rec")
    private LocalDate dtMov;

    @Column(name = "Vencto_rec")
    private LocalDate dtVencto;

    @Column(name = "DataPgto_rec")
    private LocalDate dtPgto;

    @Column(name = "Valor_rec")
    private Double valor;

    @Column(name = "ValorPago_rec")
    private Double valorPago;
}
