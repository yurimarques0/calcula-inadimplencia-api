package com.example.calculainadimplenciaapi.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;

import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.example.calculainadimplenciaapi.dto.out.ReceberDTOout;
import com.example.calculainadimplenciaapi.enums.TipoInadimplencia;
import com.example.calculainadimplenciaapi.filters.ReceberFilter;
import com.example.calculainadimplenciaapi.repositories.ReceberRepository;

@ExtendWith(SpringExtension.class)
@DisplayName("Testes da classe Inadimplencia")
public class InadimplenciaTest {
        @MockBean
        private ReceberRepository receberRepository;

        @SpyBean
        private Inadimplencia inadimplencia;

        @Nested
        class TestSomar {
                @Test
                @DisplayName("deve retornar a soma dos titulos")
                void testSomar_should_be_return_a_sum_of_titulos() {
                        List<ReceberDTOout> titulos = List.of(
                                        ReceberDTOout.builder()
                                                        .valor(10.4D)
                                                        .build(),
                                        ReceberDTOout.builder()
                                                        .valor(12.3D)
                                                        .build(),
                                        ReceberDTOout.builder()
                                                        .valor(5D)
                                                        .build());
                        Double somatoria = inadimplencia.somar(titulos);
                        assertEquals(27.7D, somatoria, 0.00001);
                }

                @Test
                @DisplayName("deve retornar o calculo da inadimplencia")
                void testCalcular_should_be_return_o_calculo_da_inadimplencia() {
                        Double titulosEmAberto = 7.0;
                        Double totalReceber = 49.0;
                        Double calculoInadimplencia = inadimplencia.calcular(titulosEmAberto, totalReceber);
                        assertEquals(14.2857D, calculoInadimplencia, 0.0001);
                }

        }

        @Test
        @DisplayName("deve retornar o calculo da inadimplencia")
        void testExecutar_should_be_return_o_calculo_da_inadimplencia() {
                List<ReceberDTOout> titulosEmAberto = List.of(
                                ReceberDTOout.builder()
                                                .valor(0.5)
                                                .build(),
                                ReceberDTOout.builder()
                                                .valor(0.5)
                                                .build(),
                                ReceberDTOout.builder()
                                                .valor(5D)
                                                .build());

                List<ReceberDTOout> titulosReceber = List.of(
                                ReceberDTOout.builder()
                                                .valor(50D)
                                                .build(),
                                ReceberDTOout.builder()
                                                .valor(10D)
                                                .build(),
                                ReceberDTOout.builder()
                                                .valor(10D)
                                                .build());

                doReturn(titulosEmAberto).when(receberRepository).titulosEmAberto(any());
                doReturn(titulosReceber).when(receberRepository).totalReceber(any());
                Double inadimpl = inadimplencia.executar(new ReceberFilter(TipoInadimplencia.ANUAL));
                assertEquals(8.5714, inadimpl, 0.0001);
        }

}
